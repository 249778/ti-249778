Projekt strony HTML w aplikacji Flask w środowisku Python.
Strona zawiera menu do nawigacji, wykres narysowany w module matplotlib.pyplot
oraz grę tekstową z wykorzystaniem skryptów w pythonie oraz "sessions" we Flasku.

Strona działa na darmowym hostingu Heroku pod adresem: 
https://ti-249778.herokuapp.com/

Projekt należy do grupy:

Filip Majewski, nr indeksu: 249778
Jan Kozłowski, nr indeksu: 249790
Jacek Małas, nr indeksu: 221086

W projekcie zawarta jest również solucja do przejścia gry.