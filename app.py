from flask import Flask, render_template, session
from random import randint
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import io
import base64
import numpy as np
app = Flask(__name__)
app.secret_key = os.urandom(24)
def rysuj(liczba):
    t = np.arange(0., float(liczba), 0.2)
    img = io.BytesIO()
    #plt.plot(t, [liczba,1], 'r')
    plt.plot(t, t, 'r', t, t**2, 'b', t, t**3, 'g')
    plt.xlabel('x')
    plt.ylabel('y')
    
    plt.savefig(img, format='png')
    img.seek(0)
    url = base64.b64encode(img.getvalue()).decode()
    plt.close()
    return 'data:image/png;base64,{}'.format(url)



@app.route("/")
def hello():
    return render_template('home.html')
    

@app.route("/about")
def about():
    rysunek = rysuj(8)
    return render_template('about.html', rysunek = rysunek)

@app.route("/game")
def game():
    return render_template('game.html')

@app.route("/gamescr1")
def scr1():
    return render_template('GameScreen1.html')

@app.route("/d1")
def d1():
    return render_template('d1.html')

@app.route("/d2")
def d2():
    return render_template('d2.html')

@app.route("/d3")
def d3():
    return render_template('d3.html')

@app.route("/d4")
def d4():
    return render_template('d4.html')

@app.route("/z1")
def z1():
    return render_template('z1.html')

@app.route("/z2")
def z():
    return render_template('z2.html')

@app.route("/z3")
def z3():
    return render_template('z3.html')

@app.route("/z4")
def z4():
    return render_template('z4.html')

@app.route("/gamescr2")
def scr2():
    return render_template('GameScreen2.html')


@app.route("/gamescr2correct")
def scr2corr():
    return render_template('GameScreen2correct.html')

@app.route("/gamescr2incorrect")
def scr2incorr():
    return render_template('GameScreen2incorrect.html')

@app.route("/gamescr3")
def scr3():
    session['rollthedice'] = randint(1,6)
    if session.get('rollthedice') in [1, 2]:
        return render_template('death1.html', variable=str(session.get('rollthedice')))
    elif session.get('rollthedice') in [5,6]:
        return render_template('kill1.html', variable=str(session.get('rollthedice')))
    elif session.get('rollthedice') in [3,4]:
        return render_template('damaged1.html', variable=str(session.get('rollthedice')))

@app.route("/gamescr4")
def scr4():
    return render_template('GameScreen4.html')

@app.route('/unlocked')
def unlocked():
    return render_template('chestUnlocked.html')

@app.route('/locked')
def locked():
    return render_template('chestLocked.html')

@app.route("/fight")
def fight():
    if session.get('rollthedice') in [5,6]:        
        return render_template('fightWon.html')
    else:
        return render_template('fightLost.html')
    

@app.route("/run")
def run():
    return render_template('run.html')


if __name__ == '__main__':
    app.run(debug = True)